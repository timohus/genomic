$(function () {
    $('.parallaxie').parallaxie({
        speed: 0.5,
    });

    searchForm();
    $('.search-field').on('change', function(){
        searchForm();
    });
});

function searchForm(){
    var field = $('.search-field');
    if(field.val() != ''){
        field.addClass('dirty');
    }else{
        field.removeClass('dirty');
    }
}